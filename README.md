# YAJIM
# Back end part of the project
# Gradle
1. Basic scripts are placed in buildSrc folder
2. All dependencies should be handled in one dependencies.gradle file
3. build.gradle one for all modules
4. Added main code-check plugins to avoid bad smell code.
#Architecture
As a basis was taken Hexagonal Architecture pattern.
Read more http://java-design-patterns.com/blog/build-maintainable-systems-with-hexagonal-architecture/
And http://tidyjava.com/hexagonal-architecture-powerful/
#Lombock
1. Allow annotation Processors in Idea(configuration in Preferences)
2. Install lombock plugin
#Deploy
###1. BACKEND

You have two options:
1. Run deploy_backend_from_ec2.sh script on your aws instance
2. Run deploy_backend.sh on your local machine

Your application will run on port 8080, so open it or configure proxy server such as nginx/
For second option you need to define two environment variables:
1. $YAJIM_KEY_PAIR absolute path to key pair that you use to connect to your ec2 instance.
2. $YAJIM_IP_AND_USER username@ip_of_ec2_instance

###2. FRONTEND

To deploy frontend part you need to run deploy_frontend.sh, it requires two environment variables: 
1. $YAJIM_S3_URL  url of your aws s3 bucket. Such as: s3://example.com
2. $YAJIM_AWS_PROFILE your aws-cli profile name
