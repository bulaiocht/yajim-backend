package com.yajim.messenger;

import com.yajim.security.AuthServerApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Access point for spring boot app.
 */
@SpringBootApplication
@EnableAutoConfiguration
@Import({AuthServerApplication.class, UserApp.class})
@SuppressWarnings("PMD")
public class MessengerApp {
    /**
     * Trying to serve static content on different endpoint.
     *
     * @return bean of {@link WebMvcConfigurerAdapter}
     */
    @Bean
    WebMvcConfigurer configurer()    {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/client")
                        .addResourceLocations("classpath:/resources/static/");
            }
        };
    }

    /**
     * @param args arguments.
     */
    public static void main(final String[] args) {
        SpringApplication.run(MessengerApp.class, args);
    }
}
