package com.yajim.messenger.service;

import com.yajim.messenger.domain.MessageRequestDTO;
import com.yajim.messenger.domain.MessageResponseDTO;
import com.yajim.messenger.domain.XmppMessage;
import com.yajim.messenger.ports.MessageRepositoryPort;
import org.codehaus.jackson.map.ObjectMapper;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.packet.Message;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Core service based on Hexagonal Architecture.
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.UnusedPrivateField", "PMD.MethodArgumentCouldBeFinal",
        "PMD.SingularField", "PMD.LocalVariableCouldBeFinal", "PMD.ShortVariable"})//spring requires.
@Service
public class MessageService {
    /**
     * Port for connection to message repository.
     */
    private final MessageRepositoryPort port;

    /**
     * MessageService userService.
     */
    private UserService userService;

    /**
     * MessageService xmppConnectionMap.
     */
    private Map<String, AbstractXMPPConnection> xmppConnectionMap; //NOPMD could be final.

    /**
     * MessageService messagesForUsername.
     */
    private Map<String, List<XmppMessage>> messagesForUsername; //NOPMD long name.

    @Autowired
    public MessageService(MessageRepositoryPort port,
                          Map<String, AbstractXMPPConnection> xmppConnection,
                          Map<String, List<XmppMessage>> messagesForUsername) { //NOPMD long name.
        this.port = port;
        this.xmppConnectionMap = xmppConnection;
        this.messagesForUsername = messagesForUsername;
    }

    /**
     * @param message websocket text message.
     * @return convert text message to messageRequestDTO.
     */
    public MessageRequestDTO convertTextMessageToMessageDTO(TextMessage message) {
        ObjectMapper mapper = new ObjectMapper();
        MessageRequestDTO messageRequestDTO;
        try {
            messageRequestDTO = mapper.readValue(new String(message.asBytes(),
                    StandardCharsets.UTF_8), MessageRequestDTO.class);
            return messageRequestDTO;
        } catch (IOException e) {
            throw new IllegalArgumentException("Cant convert message: " + message + " to MessageRequestDTO object", e);
        }
    }

    /**
     * Send incoming message from username.
     *
     * @param message  message.
     * @param username username.
     */
    public void sendMessage(MessageRequestDTO message, String username) throws Exception { //NOPMD throws Exception.
        if (xmppConnectionMap.containsKey(username)) {
            ChatManager chatManager = ChatManager.getInstanceFor(xmppConnectionMap.get(username));
            EntityBareJid bareJid = (EntityBareJid) JidCreate.from(message.getToBareJid() + "@yajim.com");
            Chat chat = chatManager.chatWith(bareJid);
            Message xmppMessage = new Message();
            xmppMessage.setBody(message.getMessage());
            chat.send(xmppMessage);
        }
    }

    /**
     * Also mark returned new messages as old.
     *
     * @param username username.
     * @return new messages for username.
     */
    public List<MessageResponseDTO> getNewMessages(String username) {
        List<MessageResponseDTO> messages = new ArrayList<>();
        //TODO register new chatListener in this case.
        if (!messagesForUsername.containsKey(username)) {
            return messages;
        }
        List<XmppMessage> newMessages = messagesForUsername.get(username);
        newMessages.stream().filter(XmppMessage::isNew).forEach(m -> {
            messages.add(new MessageResponseDTO(m.getFromJid(), m.getMessage(), m.getDate()));
            m.setNew(false);
        });
        return messages;
    }

    /**
     * GetAllMessagesForUsername method.
     *
     * @param username username.
     * @return all messages for this username.
     */
    public List<MessageResponseDTO> getAllMessagesForUsername(String username) {
        //TODO register new chatListener in this case.
        if (!messagesForUsername.containsKey(username)) {
            return new ArrayList<>();
        }
        List<XmppMessage> newMessages = messagesForUsername.get(username);
        return newMessages.stream()
                .map(m -> new MessageResponseDTO(m.getFromJid(), m.getMessage(), m.getDate()))
                .collect(Collectors.toList());

    }

    /**
     * GetAllMessagesFromUsername method.
     *
     * @param username         input username.
     * @param chatWithUsername input chatWithUsername.
     * @return list of MessageResponseDTO.
     */
    public List<MessageResponseDTO> getAllMessagesFromUsername(String username, String chatWithUsername) {
        //TODO register new chatListener in this case.
        if (!messagesForUsername.containsKey(username)) {
            return new ArrayList<>();
        }
        List<XmppMessage> newMessages = messagesForUsername.get(username);
        return newMessages.stream()
                .filter(m -> m.getFromJid().equals(chatWithUsername))
                .map(m -> new MessageResponseDTO(m.getFromJid(), m.getMessage(), m.getDate()))
                .collect(Collectors.toList());
    }
}
