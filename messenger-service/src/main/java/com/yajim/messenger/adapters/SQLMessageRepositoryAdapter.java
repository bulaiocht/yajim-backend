package com.yajim.messenger.adapters;

import com.yajim.messenger.domain.MessageRequestDTO;
import com.yajim.messenger.ports.MessageRepositoryPort;
import com.yajim.messenger.repository.SQLMessageRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Adapter between message service and database.
 * This is an abstract layer
 */
@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.BeanMembersShouldSerialize"}) //PMD dont understand beans and Lombok.
@Repository
@Getter
@Setter
public class SQLMessageRepositoryAdapter implements MessageRepositoryPort {

    /**
     * SqlMessageRepository.
     */
    private final SQLMessageRepository repository;

    @Autowired
    public SQLMessageRepositoryAdapter(final SQLMessageRepository repository) {
        this.repository = repository;
    }

    /**
     * Select for message by id.
     * @param id message is.
     * @return XmppMessage or nothing.
     */
    @SuppressWarnings("PMD.ShortVariable") //ID is short name really???LOL
    public Optional<MessageRequestDTO> getMessageById(final Long id) {
        return null;
    }
}
