package com.yajim.messenger.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Main domain object.
 * Represents message entity
 */
@Data
@Entity
public class MessageRequestDTO {

    /**
     * Primary key for message entity.
     */
    @Id
    @GeneratedValue
    private Long id; //NOPMD id is short name, seriously???

    /**
     * Jid of a person receiving message.
     */
    private String toBareJid;

    /**
     * XmppMessage itself.
     */
    private String message;
}