package com.yajim.messenger.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;

import java.time.LocalDateTime;

/**
 * MessageResponseDTO.
 */
@Data
@AllArgsConstructor
public class MessageResponseDTO {

    /**
     * Message fromJid.
     */
    private String fromJid;

    /**
     * Message message.
     */
    private String message;

    /**
     * Message date.
     */
    private LocalDateTime date;

    @Override
    @SneakyThrows
    public String toString() {
        ObjectMapper mapper = new ObjectMapper(); //NOPMD variable could be private.
        return mapper.writeValueAsString(this);
    }
}
