package com.yajim.messenger.config;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;
import javax.servlet.http.HttpSession;

/**
 * SessionHandshakeInterceptor class.
 */
public class SessionHandshakeInterceptor implements HandshakeInterceptor {

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, //NOPMD MethodArgumentCouldBeFinal.
                                   ServerHttpResponse response, //NOPMD MethodArgumentCouldBeFinal.
                                   WebSocketHandler wsHandler, //NOPMD MethodArgumentCouldBeFinal.
                                   Map<String, Object> attributes) //NOPMD MethodArgumentCouldBeFinal.
            throws Exception { //NOPMD throws Exception.
        if (request instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request; //NOPMD.
            HttpSession session = servletRequest //NOPMD MethodArgumentCouldBeFinal.
                    .getServletRequest()
                    .getSession(false);
            if (session != null) {
                attributes.put("username", session.getAttribute("username"));
            }
        }
        //        attributes.put("username", "test"); // Comment this line and uncoment previous to make session work.
        //return false will refuse connection. Possibly check for oauth token there
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, //NOPMD MethodArgumentCouldBeFinal.
                               ServerHttpResponse response, //NOPMD MethodArgumentCouldBeFinal.
                               WebSocketHandler wsHandler, //NOPMD MethodArgumentCouldBeFinal.
                               Exception exception) { //NOPMD MethodArgumentCouldBeFinal.

    }
}
