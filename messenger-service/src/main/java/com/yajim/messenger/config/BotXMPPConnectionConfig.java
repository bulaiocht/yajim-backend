package com.yajim.messenger.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


/**
 * Class for creating bot's XMPP connection.
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize",
        "PMD.LocalVariableCouldBeFinal", "PMD.MethodArgumentCouldBeFinal",
        "PMD.LongVariable"}) //PMD don't understand spring.
@Configuration
public class BotXMPPConnectionConfig {

    /**
     * XMPP host.
     */
    @Value("${xmpp.host}")
    private String host; //NOPMD UnusedPrivateField.

    /**
     * XMPP port.
     */
    @Value("${xmpp.port}")
    private int port; //NOPMD UnusedPrivateField.

    /**
     * XMPP domain.
     */
    @Value("${xmpp.domain}")
    private String domain; //NOPMD UnusedPrivateField.

    /**
     * XMPP username.
     */
    @Value("${xmpp.username}")
    private String username; //NOPMD UnusedPrivateField.

    /**
     * XMPP password.
     */
    @Value("${xmpp.password}")
    private String password; //NOPMD UnusedPrivateField.
}
