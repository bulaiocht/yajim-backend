package com.yajim.messenger.config;

import com.yajim.messenger.controller.IncomingFromXmppMessageHandler;
import com.yajim.messenger.controller.SendToXmppMessageHandler;
import com.yajim.messenger.domain.XmppMessage;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Websocket configuration.
 */
@Configuration
@EnableWebSocket
@SuppressWarnings({"PMD"}) //PMD don't understand spring.
public class MessageWebSocketConfig implements WebSocketConfigurer {

    /**
     * Registering handlers for websocket messages.
     */
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(socketReceiveMessageHandler(), "/messages").setAllowedOrigins("*")
                .addInterceptors(httpSessionConfiguration());
        registry.addHandler(sendMessageHandler(), "/new-message").setAllowedOrigins("*")
                .addInterceptors(httpSessionConfiguration());
    }

    /**
     * Bean with handler that send messages to front-end.
     *
     * @return send message handler.
     */
    @Bean
    public IncomingFromXmppMessageHandler sendMessageHandler() {
        return new IncomingFromXmppMessageHandler();
    }

    /**
     * Bean with handler for new messages that coming from frontend.
     *
     * @return recive message handler.
     */
    @Bean
    public SendToXmppMessageHandler socketReceiveMessageHandler() {
        return new SendToXmppMessageHandler();
    }

    /**
     * Configure interceptor to add session to websocket session.
     *
     * @return session interceptor.
     */
    @Bean
    public SessionHandshakeInterceptor httpSessionConfiguration() {
        return new SessionHandshakeInterceptor();
    }


    /**
     * Map with key username and value xmppConnection.
     * Contains all valid xmppConnections.
     *
     * @return map with xmpp connections.
     */
    @Bean
    public Map<String, AbstractXMPPConnection> xmppConnectionMap() {
        return new ConcurrentHashMap<>();
    }

    /**
     * Map with key username and value all messages for this username.
     *
     * @return all messages for all chats and users.
     */
    @Bean
    public Map<String, List<XmppMessage>> messagesForUsername() {
        return new ConcurrentHashMap<>();
    }
}
