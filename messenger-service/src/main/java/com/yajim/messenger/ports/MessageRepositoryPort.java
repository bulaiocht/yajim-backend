package com.yajim.messenger.ports;

import com.yajim.messenger.domain.MessageRequestDTO;

import java.util.Optional;

/**
 * An abstraction of DB communication.
 * Secondary ports in accordance Hexagonal Architecture.
 */
public interface MessageRepositoryPort {
    /**
     * Select message by id.
     *
     * @param id message id.
     * @return optional message.
     */
    Optional<MessageRequestDTO> getMessageById(Long id); //NOPMD short name id.
}
