package com.yajim.messenger.controller;

import com.yajim.messenger.domain.MessageResponseDTO;
import com.yajim.messenger.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.List;

/**
 * Handler for incoming xmpp messages.
 */
@EnableScheduling
public class IncomingFromXmppMessageHandler extends TextWebSocketHandler {

    /**
     * WebSocketSession.
     */
    private WebSocketSession webSocketSession; //NOPMD unused private field (WTF!?).

    /**
     * MessageService.
     */
    @Autowired
    private MessageService messageService; //NOPMD Found non-transient, non-static member.

    @Override
    protected void handleTextMessage(WebSocketSession session, //NOPMD could be final.
                                     TextMessage message) //NOPMD could be final.
            throws Exception { //NOPMD throws Exception.
        String username = (String) session.getAttributes().get("username"); //NOPMD could be final.
        List<MessageResponseDTO> messages = messageService.getNewMessages(username); //NOPMD could be final.
        session.sendMessage(new TextMessage(messages.toString()));
    }
}
