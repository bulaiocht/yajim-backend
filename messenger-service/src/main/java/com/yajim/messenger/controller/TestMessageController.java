package com.yajim.messenger.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * TestMessageController.
 */
@Controller
public class TestMessageController {

    /**
     * TestMessages mapping.
     *
     * @return "chat_example" view.
     */
    @GetMapping("/test/messages") //NOPMD JUnit 4 tests that execute tests should use the @Test annotation.
    public String testMessages() {
        return "chat_example";
    }

    /**
     * TestLogin mapping.
     *
     * @return "login_example" view.
     */
    @GetMapping("/test/login") //NOPMD JUnit 4 tests that execute tests should use the @Test annotation.
    public String testLogin() {
        return "login_example";
    }
}
