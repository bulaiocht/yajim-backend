package com.yajim.messenger.controller;

import com.yajim.messenger.domain.MessageResponseDTO;
import com.yajim.messenger.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.servlet.http.HttpSession;

/**
 * Controller for connections with xmpp server.
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize",
        "PMD.LocalVariableCouldBeFinal", "PMD.MethodArgumentCouldBeFinal",
        "PMD.ImmutableField"}) //PMD don't understand spring.
@RestController
@RequestMapping(path = "/chats")
public class MessageController {

    /**
     * MessageService.
     */
    @Autowired
    private MessageService messageService;

    /**
     * GetAllMessages mapping.
     *
     * @param httpSession input http session.
     * @return list of MessageResponseDTO.
     */
    @GetMapping("/")
    public List<MessageResponseDTO> getAllMessages(HttpSession httpSession) {
        String currentUsername = (String) httpSession.getAttribute("username");
        return messageService.getAllMessagesForUsername(currentUsername);
    }

    /**
     * GetAllMessagesForChat mapping.
     *
     * @param chatWithUsername input chatWithUsername.
     * @param httpSession      input httpSession.
     * @return list of MessageResponseDTO.
     */
    @GetMapping("/{chatWithUsername}")
    public List<MessageResponseDTO> getAllMessagesForChat(@PathVariable String chatWithUsername,
                                                          HttpSession httpSession) {
        String fromUsername = (String) httpSession.getAttribute("username");
        return messageService.getAllMessagesFromUsername(fromUsername, chatWithUsername);

    }
}