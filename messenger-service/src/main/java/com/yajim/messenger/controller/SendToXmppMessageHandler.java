package com.yajim.messenger.controller;

import com.yajim.messenger.domain.MessageRequestDTO;
import com.yajim.messenger.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Socket controller.
 */
@SuppressWarnings("PMD.MethodArgumentCouldBeFinal")
public class SendToXmppMessageHandler extends TextWebSocketHandler {

    /**
     * MessageService.
     */
    @Autowired
    private MessageService messageService; //NOPMD Found non-transient, non-static member.

    /**
     * Simple message processor.
     *
     * @param session     input session.
     * @param textMessage input textMessage.
     */
    @Override
    public void handleTextMessage(WebSocketSession session,
                                  TextMessage textMessage)
            throws Exception { //NOPMD throws Exception.
        MessageRequestDTO message = messageService.convertTextMessageToMessageDTO(textMessage); //NOPMD could be final.
        String fromJid = (String) session.getAttributes().get("username"); //NOPMD could be final.
        messageService.sendMessage(message, fromJid);
    }
}
