package com.yajim.messenger.repository;

import com.yajim.messenger.domain.MessageRequestDTO;
import org.springframework.data.repository.CrudRepository;

/**
 * SQL message repository.
 */
public interface SQLMessageRepository extends CrudRepository<MessageRequestDTO, Long> {
}
