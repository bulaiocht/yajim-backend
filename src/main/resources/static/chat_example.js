$(document).ready(function () {
    // открываем  конекшин на отправку сообщений.
    var sock = new WebSocket("ws://localhost:8080/messages");
    // открываем конекшин на получение сообщений.
    var sock1 = new WebSocket("ws://localhost:8080/new-message");


    /**
     *
     * получаем масив с собщениям прасим его в цыкле вытягивая от кого, сообщение, время отправки.
     * @param e джейсон который пришол.
     */
    sock1.onmessage = function(e) {
        console.log(e.data);
        var content = JSON.parse(e.data);
        // console.log(content); посмотри что приходить надо грамотно роспасить дату прихода сообщения.
        $old_content = $("#chat_content").html();
        $new_line = "";
        for(i =0; i < content.length; i++) {
            $new_line+="User " + content[i].fromJid + ": " + content[i].message + "<br />";
        }
        console.log($new_line);
        $("#chat_content").html($old_content + $new_line);
    };

    /**
     * Отправляем новое сообщения, составляем джейсон с формы.
     */
    $("#submit_btn").on("click", function() {
        var toId = $("#toJid").val();
        var message = $("#message").val();
        var send_data  = {
            toBareJid: toId,
            messageBody: message
        };
        sock.send(JSON.stringify(send_data));
    });
    /**
     * Каждые 10 секунд мы отправляем "ping", это сообщения запускает проверку на новые сообщения,
     * если они есть то они возвращяются ввиде джэйсон масива.
     */
    setInterval(function () {
        sock1.send("ping")
    },1000)
});