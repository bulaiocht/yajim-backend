#!/bin/bash

#Script that deploy frontend part of our project to AWS S3 bucket
#To run from root directory of project do "source deploy/deploy_frontend.sh"

#Usefull constants
scriptDirectory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Check requirements
source $scriptDirectory"/check_dependencies.sh"
ERROR=$?
if [ "$ERROR" -eq 1 ]; then
return ;
fi

#Clone git projects. Note that this will create directory in your user home directory.
cd ~/
mkdir -p www/data
cd ~/www/data
git clone https://EVOBot:evobot123@bitbucket.org/bulaiocht/yajim-front.git

#Build project
cd yajim-front/
git checkout dev
npm install
gulp build

#Deploy to S3. Note you must have pre-configured user that have access to bucket.
cd build
aws s3 sync . $YAJIM_S3_URL --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --profile $YAJIM_AWS_PROFILE