#!/bin/bash

#Common dependencies
type aws >/dev/null 2>&1 || { echo >&2 "AWS is not installed. Aborting."; return 1; }
type git >/dev/null 2>&1 || { echo >&2 "GIT is not installed. Aborting."; return 1; }
type java >/dev/null 2>&1 || { echo >&2 "Java is not installed. Aborting."; return 1; }
type git >/dev/null 2>&1 || { echo >&2 "GIT is not installed. Aborting."; return 1; }
type gradle >/dev/null 2>&1 || { echo >&2 "Gradle is not installed. Aborting."; return 1; }
type npm >/dev/null 2>&1 || { echo >&2 "NPM is not installed. Aborting."; return 1; }
type gulp >/dev/null 2>&1 || { echo >&2 "Gulp is not installed. Aborting."; return 1; }
return 0;