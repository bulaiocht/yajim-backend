#!/bin/bash

#Deploy backend part of project to AWS EC2.
#To run from root directory of project do "source deploy/deploy_backend.sh"

#Usefull constants
scriptDirectory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Common dependencies
type git >/dev/null 2>&1 || { echo >&2 "GIT is not installed. Aborting."; exit 1; }
type java >/dev/null 2>&1 || { echo >&2 "Java is not installed. Aborting."; exit 1; }
type gradle >/dev/null 2>&1 || { echo >&2 "Gradle is not installed. Aborting."; exit 1; }

#Clone git projects. Note that this will create directory in your user home directory.
cd ~/
mkdir -p www/data
cd www/data
git clone https://EVOBot:evobot123@bitbucket.org/bulaiocht/yajim-backend.git

#Build project
cd yajim-backend
gradle clean build

#Run jar on server
cd build/libs/
nohup java -jar *exec.jar