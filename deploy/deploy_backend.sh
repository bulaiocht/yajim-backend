#!/bin/bash

#Deploy backend part of project to AWS EC2.
#To run from root directory of project do "source deploy/deploy_backend.sh"

#Usefull constants
scriptDirectory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Check requirements source
source $scriptDirectory"/check_dependencies.sh"
ERROR=$?
if [ "$ERROR" -eq 1 ]; then
return ;
fi

#Clone git projects. Note that this will create directory in your user home directory.
cd ~/
mkdir -p www/data
cd www/data
git clone https://EVOBot:evobot123@bitbucket.org/bulaiocht/yajim-backend.git

#Build project
cd yajim-backend
#Test and checkCoverage disabled!
gradle clean build
#Deploy jar to server
scp -i $YAJIM_KEY_PAIR build/libs/*exec.jar $YAJIM_IP_AND_USER:~/www/data/tmp/
ssh -i $YAJIM_KEY_PAIR $YAJIM_IP_AND_USER /home/ubuntu/bin/recive_new_jar.sh