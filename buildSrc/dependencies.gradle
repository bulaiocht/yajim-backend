apply plugin: "io.spring.dependency-management"
apply plugin: "net.saliman.properties"
apply plugin: 'nebula.optional-base'
apply plugin: 'org.springframework.boot'

extensions.create("shared", SharedDependencies)

class SharedDependencies {
    def springBootVersion = "1.5.6.RELEASE"

    Closure globalJavaConfig = {
        apply plugin: 'java'
        apply plugin: 'idea'
        sourceCompatibility = 1.8
        targetCompatibility = 1.8
    }

    Closure globalRepositories = {
        repositories {
            mavenCentral()
            jcenter()
            maven {
                url 'http://repo.spring.io/milestone'
            }
        }
    }

    Closure globalDependencyManagement = {
        resolutionStrategy {
            cacheChangingModulesFor 0, 'seconds'
            cacheDynamicVersionsFor 0, 'seconds'
        }
    }

    Closure applicationDependencies = {
        compile group: 'org.projectlombok', name: 'lombok', version: '1.16.18'
        compile group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: '2.8.9'
        compile group: "org.springframework.boot", name: "spring-boot-starter-web", version: springBootVersion
        compile group: "org.springframework.boot", name: "spring-boot-starter-thymeleaf", version: springBootVersion
        compile group: "org.springframework.boot", name: "spring-boot-starter-tomcat", version: springBootVersion
        compile group: 'org.springframework.boot', name: 'spring-boot-devtools', version: springBootVersion
        compile group: 'org.springframework.boot', name: 'spring-boot-starter-data-jpa', version: springBootVersion
        compile group: 'org.springframework.boot', name: 'spring-boot-configuration-processor', version: springBootVersion
        compile group: 'org.springframework.boot', name: 'spring-boot-starter-websocket', version: springBootVersion
        //compile group: 'org.springframework.boot', name: 'spring-boot-starter-data-redis', version: springBootVersion
        compile group: 'org.springframework.session', name: 'spring-session-jdbc', version: '1.3.1.RELEASE'
        compile group: 'com.hazelcast', name: 'hazelcast', version: '3.8.5'
        compile group: 'org.webjars', name: 'webjars-locator', version: '0.32-1'
        compile group: 'org.webjars', name: 'sockjs-client', version: '1.1.2'
        compile group: 'org.webjars', name: 'stomp-websocket', version: '2.3.3-1'
        compile group: 'org.webjars', name: 'bootstrap', version: '3.3.7-1'
        compile group: 'org.webjars', name: 'jquery', version: '3.2.1'
        compile group: 'org.logback-extensions', name: 'logback-ext-loggly', version: '0.1.4'
        compile group: 'org.springframework.boot', name: 'spring-boot-starter-log4j2', version: springBootVersion
        compile group: 'org.springframework.boot', name: 'spring-boot-starter-actuator', version: springBootVersion

    }

    Closure securityDependencies = {
        compile group: 'org.springframework.security', name: 'spring-security-messaging', version: '4.0.1.RELEASE'
        compile group: 'org.springframework.boot', name: 'spring-boot-starter-security', version: springBootVersion
        compile group: 'org.springframework.security.oauth', name: 'spring-security-oauth2', version: '2.1.1.RELEASE'
        compile group: 'org.springframework.security', name: 'spring-security-jwt', version: '1.0.8.RELEASE'
    }


    Closure dataBaseDependencies = {
        compile group: 'org.springframework.boot', name: 'spring-boot-starter-data-jpa', version: springBootVersion
        compile group: 'com.h2database', name: 'h2', version: '1.4.196'
        compile group: 'org.hibernate', name: 'hibernate-validator', version: '5.4.1.Final'
        compile group: 'org.postgresql', name: 'postgresql', version: '42.1.4'
    }

    Closure unitTestDependencies = {
        testCompile group: "junit", name: "junit", version: "4.12"
        testCompile group: "org.assertj", name: "assertj-core", version: "3.3.0"
        testCompile group: "org.hamcrest", name: "hamcrest-core", version: "1.3"
        testCompile group: "org.hamcrest", name: "hamcrest-library", version: "1.3"
        testCompile group: "org.mockito", name: "mockito-core", version: "2.7.22"
        testCompile group: 'org.mockito', name: 'mockito-all', version: '1.10.19'
        testCompile group: 'com.jayway.restassured', name: 'spring-mock-mvc', version: '2.9.0'
        testCompile group: 'org.glassfish', name: 'javax.el', version: '3.0.0'
        testCompile group: 'org.skyscreamer', name: 'jsonassert', version: '0.9.0'
        testCompile group: 'org.springframework.boot', name: 'spring-boot-starter-test', version: springBootVersion
        compile group: 'org.springframework.boot', name: 'spring-boot-starter-actuator', version: springBootVersion

    }

    Closure xmppDependencies = {
        compile group: "org.igniterealtime.smack", name: "smack-java7", version: "4.2.0"
        // Optional for XMPPTCPConnection
        compile "org.igniterealtime.smack:smack-tcp:4.2.0"
        // Optional for XMPP-IM (RFC 6121) support (Roster, Threaded Chats, …)
        compile "org.igniterealtime.smack:smack-im:4.2.0"
        // Optional for XMPP extensions support
        compile "org.igniterealtime.smack:smack-extensions:4.2.0"
        // https://mvnrepository.com/artifact/org.igniterealtime.smack/smack-bosh
        compile "org.igniterealtime.smack:smack-bosh:4.2.0"
    }
}