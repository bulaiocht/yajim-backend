package com.yajim.messenger;

import com.yajim.messenger.domain.Role;
import com.yajim.messenger.domain.YajimUser;
import com.yajim.messenger.dto.LoginRequestDTO;
import com.yajim.messenger.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceTest {

    @TestConfiguration
    static class UserServiceContextTestConfig {
        @Bean
        public UserService getUserService (){
            return new UserService();
        }
    }

    @Autowired
    private UserService userService;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    public void userServiceTest(){

        YajimUser userOne = this.testEntityManager.persist(
                new YajimUser("test_user1","test_user1",
                        new HashSet<>(Arrays.asList(new Role("ROLE_USER")))));

        YajimUser userTwo = this.testEntityManager.persist(
                new YajimUser("test_user2","test_user2",
                        new HashSet<>(Arrays.asList(new Role("ROLE_USER")))));

        YajimUser userAdmin = this.testEntityManager.persist(
                new YajimUser("test_admin","test_admin",
                        new HashSet<>(Arrays.asList(new Role("ROLE_ADMIN")))));


        LoginRequestDTO trueRequest = new LoginRequestDTO();
        trueRequest.setUsername("test_user1");
        trueRequest.setPassword("test_user1");

        LoginRequestDTO falseRequest = new LoginRequestDTO();
        falseRequest.setUsername("not_user1");
        falseRequest.setPassword("not_user1");

        boolean userIsPresent = userService.userIsPresent(trueRequest);
        System.out.println(userService.getByUsername(trueRequest));
        boolean userIsNotPresent = userService.userIsPresent(falseRequest);
        System.out.println(userService.getByUsername(falseRequest));
        assertTrue(userIsPresent);
        assertFalse(userIsNotPresent);
    }
}

