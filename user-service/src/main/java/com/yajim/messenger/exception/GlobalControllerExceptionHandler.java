package com.yajim.messenger.exception;

import com.yajim.messenger.dto.ValidationErrorResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.http.HttpServletRequest;

/**
 * Spring global exception controller.
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {

    /**
     * Response for validation exception.
     * @param request http request.
     * @param errors errors that happened.
     * @return validation error response in json format.
     */
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ValidationErrorResponseDTO methodArgumentNotValidExceptionHandler(final HttpServletRequest request,
                                                                      final Errors errors) {
        final LocalDateTime now = LocalDateTime.now();
        final String timestamp = now.format(DateTimeFormatter.ofPattern("dd/mm/yyyy HH:mm:ss"));
        final ValidationErrorResponseDTO response = new ValidationErrorResponseDTO();
        response.setTimestamp(timestamp);
        response.setMessage(errors.getFieldError().getDefaultMessage());
        response.setApplicationMessage("Refused, object is not valid.");
        response.setException("Validation exception.");
        response.setStatus("400");
        response.setPath(request.getServletPath());
        return response;
    }
}
