package com.yajim.messenger.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Property class for reading XMPP properties.
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "xmpp")
@Component
public class XMPPConfigProperties {

    /**
     * XMPP host from property file.
     */
    private String host;

    /**
     * XMPP port from property file.
     */
    private int port;

    /**
     * XMPP domain from property file.
     */
    private String domain;

    /**
     * Username.
     */
    private String username;

    /**
     * Password.
     */
    private String password;

}