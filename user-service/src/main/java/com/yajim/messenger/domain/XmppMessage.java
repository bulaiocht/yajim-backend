package com.yajim.messenger.domain;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * XmppMessage class.
 */
@Data
public class XmppMessage {

    /**
     * XmppMessage fromJid.
     */
    private String fromJid;

    /**
     * XmppMessage message.
     */
    private String message;

    /**
     * XmppMessage isNew.
     */
    private boolean isNew; //NOPMD Found non-transient, non-static member.

    /**
     * XmppMessage date.
     */
    private LocalDateTime date;

    /**
     * Setting message parameter new.
     *
     * @param aNew parameter new.
     */
    public synchronized void setNew(boolean aNew) { //NOPMD aNew could be final.
        isNew = aNew;
    }
}
