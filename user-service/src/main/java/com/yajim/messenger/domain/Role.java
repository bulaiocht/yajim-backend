package com.yajim.messenger.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * User role entity.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings({"PMD.ShortClassName", "PMD.ShortVariable",
        "PMD.BeanMembersShouldSerialize", "PMD.ImmutableField",
        "PMD.MethodArgumentCouldBeFinal"})
public class Role {
    /**
     * Entity's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Role name.
     * May be USER or ADMIN or both
     */
    private String name;

    public Role(String name) {
        this.name = name;
    }
}

