package com.yajim.messenger.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


/**
 * Main user entity.
 */
@Data
@Entity
@NoArgsConstructor
@Getter
@Setter
@SuppressWarnings({"PMD.ShortVariable",
        "PMD.BeanMembersShouldSerialize", "PMD.ImmutableField",
        "PMD.MethodArgumentCouldBeFinal"})
public class YajimUser {

    /**
     * YajimUser primary key.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Username.
     */
    private String username;

    /**
     * Password.
     */
    private String password;

    /**
     * Set of user roles.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();

    public YajimUser(String username, String password, Set<Role> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
}