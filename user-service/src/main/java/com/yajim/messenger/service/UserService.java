package com.yajim.messenger.service;

import com.yajim.messenger.domain.Role;
import com.yajim.messenger.domain.YajimUser;
import com.yajim.messenger.dto.LoginRequestDTO;
import com.yajim.messenger.repositories.SQLYajimUserRepository;
import lombok.NoArgsConstructor;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Service to work with users.
 * Main service based on Hexagonal Architecture.
 */
@Service
@NoArgsConstructor
@SuppressWarnings({"PMD.BeanMembersShouldSerialize",
        "PMD.MethodArgumentCouldBeFinal", "PMD.LocalVariableCouldBeFinal",
        "PMD.LongVariable"})
public class UserService {

    /**
     * XMPP host.
     */
    @Value("${xmpp.host}")
    private String host;

    /**
     * XMPP port.
     */
    @Value("${xmpp.port}")
    private int port;

    /**
     * XMPP domain.
     */
    @Value("${xmpp.domain}")
    private String domain;

    /**
     * User repository port.
     */
    private SQLYajimUserRepository userRepository; // NOPMD unused private field.

    @Autowired
    public void setUserRepository(SQLYajimUserRepository sqlYajimUserRepository) {
        this.userRepository = sqlYajimUserRepository;
    }

    /**
     * @return true if there is such user and false if there is not.
     */
    public boolean userIsPresent(LoginRequestDTO loginRequestDTO) {
        return userRepository.findByUsername(loginRequestDTO.getUsername()).isPresent();
    }

    /**
     * @param loginRequestDTO request.
     * @return instance of {@link YajimUser} anyway.
     */
    public YajimUser getByUsername(LoginRequestDTO loginRequestDTO) {
        return userRepository.findByUsername(loginRequestDTO.getUsername())
                .orElse(new YajimUser("---", "---",
                        new HashSet<>(Arrays.asList(new Role("ANON")))));
    }

    /**
     * Method for getting XMPP server connection by registered username.
     *
     * @return new xmpp connection for user by login/password.
     */
    public AbstractXMPPConnection getConnectionByUsername(String username, String password) throws IOException,
            InterruptedException, XMPPException, SmackException {
        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        configBuilder.setUsernameAndPassword(username, password)
                .setDebuggerEnabled(true)
                .setHost(host)
                .setPort(port)
                .setXmppDomain(domain)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        XMPPTCPConnection connection = new XMPPTCPConnection(configBuilder.build());
        connection.connect();
        connection.login();

        return connection;
    }
}