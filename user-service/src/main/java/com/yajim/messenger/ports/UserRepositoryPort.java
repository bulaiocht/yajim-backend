package com.yajim.messenger.ports;

import com.yajim.messenger.domain.YajimUser;

import java.util.List;
import java.util.Optional;

/**
 * An abstraction of DB communication.
 * Secondary ports in accordance Hexagonal Architecture.
 */
public interface UserRepositoryPort {

    /**
     * Select user by username.
     * @param username username.
     * @return {@link Optional} wrapper with {@link YajimUser}.
     */
    Optional<YajimUser> findByUsername(String username);

    /**
     * Method to get all users.
     * @return {@link List} of {@link YajimUser} users
     */
    List<YajimUser> findAll();

    /**
     * Method to save user.
     * @param user is user
     * @param <S> instance of {@link YajimUser} class
     * @return saved user
     */
    <S extends YajimUser> S save(S user);

}
