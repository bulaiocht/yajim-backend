package com.yajim.messenger.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO class for user registration.
 */
@Getter
@Setter
@ToString
public class RegisterRequestDTO implements RequestDTO {

    /**
     * @param username username.
     *
     */
    private String username;
    /**
     * @param password password.
     *
     */
    private String password;

}
