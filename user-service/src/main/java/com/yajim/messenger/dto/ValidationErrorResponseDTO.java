package com.yajim.messenger.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.NoArgsConstructor;

/**
 * Validation error response.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@SuppressWarnings({"PMD.LongVariable",
        "PMD.MethodArgumentCouldBeFinal"})
public class ValidationErrorResponseDTO {

    /**
     * @param timestamp current time.
     */
    private String timestamp;

    /**
     * @param status erorr status(HTTP).
     */
    private String status;

    /**
     * @param error error that happened.
     */
    private String error;

    /**
     * @param exception Spring exception.
     */
    private String exception;

    /**
     * @param message human readable message.
     */
    private String message;

    /**
     * @param path url path.
     */
    private String path;

    /**
     * @param applicationMessage Spring message.
     */
    private String applicationMessage;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getApplicationMessage() {
        return applicationMessage;
    }

    public void setApplicationMessage(String applicationMessage) {
        this.applicationMessage = applicationMessage;
    }
}
