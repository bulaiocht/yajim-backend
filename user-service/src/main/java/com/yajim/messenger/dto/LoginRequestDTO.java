package com.yajim.messenger.dto;

import lombok.ToString;

/**
 * Login request class.
 */
@ToString
@SuppressWarnings({"PMD.MethodArgumentCouldBeFinal"})
public class LoginRequestDTO implements RequestDTO {

    /**
     * @param username username.
     *
     */
    private String username;

    /**
     * @param password password.
     *
     */
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
