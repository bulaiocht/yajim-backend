package com.yajim.messenger.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Login Response template.
 */
@Getter
@Setter
@ToString
public class LoginResponseDTO {

    /**
     * Generated errors for user field.
     */
    private List<String> userFieldError;

    /**
     * Generated errors for password field.
     */
    private List<String> passFieldError;
}
