package com.yajim.messenger.adapters;

import com.yajim.messenger.domain.YajimUser;
import com.yajim.messenger.ports.UserRepositoryPort;
import com.yajim.messenger.repositories.SQLYajimUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Adapter between message service and database.
 */
@Repository
@SuppressWarnings({"PMD.MethodArgumentCouldBeFinal"})
public class SQLYajimUserRepositoryAdapter implements UserRepositoryPort {

    /**
     * SQLYajimUserRepository connection.
     */
    private SQLYajimUserRepository userRepository; //NOPMD getters setters.

    @Autowired
    public SQLYajimUserRepositoryAdapter(SQLYajimUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<YajimUser> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<YajimUser> findAll() {
        return userRepository.findAll();
    }

    @Override
    public <S extends YajimUser> S save(S user) {
        if (user != null) {
            userRepository.save(user);
        }
        return  user;
    }

}
