package com.yajim.messenger;

import com.yajim.messenger.domain.Role;
import com.yajim.messenger.domain.YajimUser;
import com.yajim.messenger.repositories.SQLYajimUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Spring boot application.
 */
@SpringBootApplication
@SuppressWarnings({"PMD.UseUtilityClass",
        "PMD.AnnotationUseStyle", "PMD.MethodArgumentCouldBeFinal",
        "PMD.LocalVariableCouldBeFinal"})
public class UserApp {

    @Bean
    @Autowired
    CommandLineRunner runner(SQLYajimUserRepository repository) {

        YajimUser user = new YajimUser();
        user.setUsername("user");
        user.setPassword("user");
        user.getRoles().add(new Role("ROLE_USER"));

        YajimUser botOne = new YajimUser();
        botOne.setUsername("botabcde1");
        botOne.setPassword("botabcde1");
        botOne.getRoles().add(new Role("ROLE_USER"));

        YajimUser botTwo = new YajimUser();
        botTwo.setUsername("botabcde2");
        botTwo.setPassword("botabcde2");
        botTwo.getRoles().add(new Role("ROLE_USER"));

        YajimUser admin = new YajimUser();
        admin.setUsername("admin");
        admin.setPassword("admin");
        admin.getRoles().add(new Role("ROLE_ADMIN"));

        return args -> {
            repository.save(user);
            repository.save(admin);
            repository.save(botOne);
            repository.save(botTwo);
        };
    }

    /**
     * @param args arguments.
     */
    public static void main(final String[] args) {
        SpringApplication.run(UserApp.class, args);
    }
}
