package com.yajim.messenger.repositories;

import com.yajim.messenger.domain.YajimUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository to get user from SQL DB.
 */
@Repository
public interface SQLYajimUserRepository extends JpaRepository<YajimUser, Long> {

    /**
     * Method to find user by username.
     * @param username username.
     * @return {@link Optional}
     */
    Optional<YajimUser> findByUsername(String username);

    /**
     * Method to find all users.
     * @return {@link List} of users.
     */
    List<YajimUser> findAll();

    /**
     * Method to save user to DB.
     * @param entity of {@link YajimUser}
     * @param <S> instance of {@link YajimUser} class
     * @return saved user.
     */
    <S extends YajimUser> S save(S entity);

}