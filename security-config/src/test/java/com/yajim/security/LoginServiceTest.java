package com.yajim.security;


import com.yajim.messenger.dto.LoginRequestDTO;
import com.yajim.security.exceptions.AuthenticationException;
import com.yajim.security.service.LoginService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;


public class LoginServiceTest {

    private static LoginService loginService;
    private static TokenEndpoint endpoint;

    @BeforeClass
    public static void init() {
        endpoint = mock(TokenEndpoint.class);
        loginService = new LoginService(endpoint);
    }

    @Test
    public void testThatLoginServiceAuthorizeUserWithValidParameters() throws HttpRequestMethodNotSupportedException {
        LoginRequestDTO request = new LoginRequestDTO();
        request.setUsername("user");
        request.setPassword("password123");
        doReturn(new ResponseEntity<OAuth2AccessToken>(HttpStatus.OK)).when(endpoint).postAccessToken(any(), any());
        Assert.assertEquals(200, loginService.authorizeUser(request).getStatusCodeValue());
    }


    @Test(expected = AuthenticationException.class)
    public void testThatLoginServiceFailsToAuthorizeForInvalidUser() throws HttpRequestMethodNotSupportedException {
        LoginRequestDTO request = new LoginRequestDTO();
        request.setUsername("user");
        request.setPassword("password123");
        doThrow(new RuntimeException()).when(endpoint).postAccessToken(any(), any());
        loginService.authorizeUser(request);
    }
}
