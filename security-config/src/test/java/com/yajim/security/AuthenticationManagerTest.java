//package com.yajim.security;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@ActiveProfiles("test")
//@RunWith(SpringRunner.class)
//@AutoConfigureMockMvc
//@SpringBootTest
//public class AuthenticationManagerTest {
//    private final String URL = "/signin";
//
//    @Autowired
//    private WebApplicationContext wac;
//
//    private MockMvc mockMvc;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
//    }
//
//    @Test
//    public void testAuthenticationWrongLogin() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","botabcd2")
//                .param("password","botabcde2")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//    }
//
//    @Test
//    public void testAuthenticationWrongPassword() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","botabcde2")
//                .param("password","botabcd2")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//    }
//
//    @Test
//    public void testAuthenticationUsernameEmptyOrNull() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","")
//                .param("password","botabcde2")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//
//        result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("password","botabcde2")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//
//    }
//
//    @Test
//    public void testAuthenticationShortUsername() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","123")
//                .param("password","botabcde2")
//                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
//                .andExpect(status().isBadRequest());
//
//    }
//
//    @Test
//    public void testAuthenticationLongUsername() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","0123456789abcdefghijk")
//                .param("password","botabcde2")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//
//    }
//    @Test
//    public void testAuthenticationWrongchUsername() throws Exception {
//
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","den#hl")
//                .param("password","botabcde2")
//                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
//                .andExpect(status().isBadRequest());
//
//    }
//
//    @Test
//    public void testAuthenticationPasswordEmptyOrNull() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","botabcde2")
//                .param("password","")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//        result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","botabcde2")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//    }
//
//    @Test
//    public void testAuthenticationShortPassword() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","botabcde2")
//                .param("password","1234567")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//
//    }
//
//    @Test
//    public void testAuthenticationLongPassword() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","botabcde2")
//                .param("password","1234567890123456789012345678901234567890"
//                        +
//                        "1234567890123456789012345678901234567890")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//
//    }
//    @Test
//    public void testAuthenticationWrongchPassword() throws Exception {
//        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URL)
//                .param("username","botabcde2")
//                .param("password","123@#4567")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
//    }
//
//}
//
//
//
