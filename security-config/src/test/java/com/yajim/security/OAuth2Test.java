//package com.yajim.security;
//
//import org.codehaus.jackson.map.ObjectMapper;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.context.embedded.LocalServerPort;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.jwt.Jwt;
//import org.springframework.security.jwt.JwtHelper;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.List;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//@ActiveProfiles("test")
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {AuthServerApplication.class},
//        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//public class OAuth2Test {
//
//    @LocalServerPort
//    private int port;
//
//    /**
//     * Method for testing token generation for User
//     * @throws IOException if something wrong with I/O
//     */
//    @SuppressWarnings({"rawtypes", "unchecked"})
//    @Test
//    public void getJwtTokenByClientCredentialForUser() throws IOException {
//        ResponseEntity<String> response = new TestRestTemplate("trusted-app", "secret")
//                .postForEntity("http://localhost:" + port +
//                                "/oauth/token?grant_type=password&username=botabcde1&password=botabcde1",
//                        null, String.class);
//        String responseText = response.getBody();
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        HashMap jwtMap = new ObjectMapper().readValue(responseText, HashMap.class);
//
//        assertEquals("bearer", jwtMap.get("token_type"));
//        assertEquals("read write", jwtMap.get("scope"));
//        assertTrue(jwtMap.containsKey("access_token"));
//        assertTrue(jwtMap.containsKey("expires_in"));
//        assertTrue(jwtMap.containsKey("jti"));
//        String accessToken = (String) jwtMap.get("access_token");
//
//        Jwt jwtToken = JwtHelper.decode(accessToken);
//        String claims = jwtToken.getClaims();
//        HashMap claimsMap = new ObjectMapper().readValue(claims, HashMap.class);
//
//        //assertEquals("spring-boot-application", ((List<String>)claimsMap.get("aud")).get(0));
//        assertEquals("trusted-app", claimsMap.get("client_id"));
//        assertEquals("botabcde1", claimsMap.get("user_name"));
//        assertEquals("read", ((List<String>) claimsMap.get("scope")).get(0));
//        assertEquals("write", ((List<String>) claimsMap.get("scope")).get(1));
//        assertEquals("ROLE_USER", ((List<String>) claimsMap.get("authorities")).get(0));
//    }
//
//    /**
//     * Method for testing access restrictions for User
//     * @throws IOException
//     */
//    @Test
//    public void accessProtectedResourceWithJwtTokenByUser() throws IOException {
//
//        //testing unprotected resource
//        ResponseEntity<String> response = new TestRestTemplate().getForEntity("http://localhost:" + port + "/public", null, String.class);
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//
//        //testing for unauthorized access
//        response = new TestRestTemplate().getForEntity("http://localhost:" + port + "/private", null, String.class);
//        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
//
//        //getting valid token
//        response = new TestRestTemplate("trusted-app", "secret")
//                .postForEntity("http://localhost:" + port +
//                                "/oauth/token?grant_type=password&username=botabcde1&password=botabcde1",
//                        null, String.class);
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        String responseText = response.getBody();
//
//        HashMap jwtMap = new ObjectMapper().readValue(responseText, HashMap.class);
//        String accessToken = (String) jwtMap.get("access_token");
//
//        HttpHeaders httpHeader = new HttpHeaders();
//        httpHeader.set("Authorization", "Bearer " + accessToken);
//
//        response = new TestRestTemplate().exchange("http://localhost:" + port + "/private",
//                HttpMethod.GET, new HttpEntity<>(null, httpHeader)
//                , String.class);
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//
//    }
//
//}
//
