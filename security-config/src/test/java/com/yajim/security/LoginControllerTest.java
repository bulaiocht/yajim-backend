//package com.yajim.security;
//
//import com.yajim.messenger.domain.UserSession;
//import com.yajim.messenger.dto.LoginRequestDTO;
//import com.yajim.messenger.service.UserService;
//import com.yajim.messenger.validation.LoginRequestDTOValidation;
//import com.yajim.security.controller.LoginController;
//import com.yajim.security.service.LoginService;
//import org.jivesoftware.smack.SmackException;
//import org.jivesoftware.smack.XMPPException;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.oauth2.common.OAuth2AccessToken;
//import org.springframework.web.bind.WebDataBinder;
//
//import java.io.IOException;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doNothing;
//import static org.mockito.Mockito.doReturn;
//import static org.mockito.Mockito.doThrow;
//import static org.mockito.Mockito.mock;
//
///**
// * Test for login controller.
// */
//public class LoginControllerTest {
//
//    private static LoginController loginController;
//    private static UserSession userSession;
//    private static UserService userService;
//    private static LoginService loginService;
//    private LoginRequestDTO requestDTO;
//
//    @BeforeClass
//    public static void init() {
//        userSession = mock(UserSession.class);
//        userService = mock(UserService.class);
//        loginService = mock(LoginService.class);
//        loginController = new LoginController(userService, loginService, userSession);
//    }
//
//    @Before
//    public void reInit() {
//        requestDTO = new LoginRequestDTO();
//    }
//
//    @Test
//    public void testThatLoginControllerReturn200IfSuccessfullyOpenXMPPConnection() {
//        doReturn(true).when(userService).userIsPresent(requestDTO);
//        doNothing().when(userSession).setXmppConnection(any());
//        doReturn(new ResponseEntity<OAuth2AccessToken>(HttpStatus.OK)).when(loginService).authorizeUser(any());
//        Assert.assertEquals(200, loginController.authenticateUser(requestDTO).getStatusCodeValue());
//    }
//
//    @Test
//    public void testThatLoginControllerReturn400IfUserIsNotPresent() {
//        doReturn(false).when(userService).userIsPresent(requestDTO);
//        Assert.assertEquals(400, loginController.authenticateUser(requestDTO).getStatusCodeValue());
//    }
//
//    @Test
//    public void testThatLoginControllerReturn500WhenCantOpenXMPPConnection() throws InterruptedException, XMPPException,
//            SmackException, IOException {
//        doReturn(true).when(userService).userIsPresent(requestDTO);
//        doThrow(new SmackException("")).when(userService).getConnectionByUsername(any(), any());
//        Assert.assertEquals(500, loginController.authenticateUser(requestDTO).getStatusCodeValue());
//    }
//
//    @Test
//    public void testThatBinderBindToLoginControllerValidation() {
//        WebDataBinder webDataBinder = new WebDataBinder(null);
//        loginController.initBinder(webDataBinder);
//        Assert.assertEquals(LoginRequestDTOValidation.class, webDataBinder.getValidator().getClass());
//    }
//
//}
