package com.yajim.security.adapters;

import com.yajim.messenger.domain.Role;
import com.yajim.messenger.domain.YajimUser;
import com.yajim.messenger.repositories.SQLYajimUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link UserDetailsService} interface.
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize",
        "PMD.ImmutableField", "PMD.MethodArgumentCouldBeFinal",
        "PMD.LongVariable", "PMD.LocalVariableCouldBeFinal"})
public class YajimUserDetailsServiceAdapter implements UserDetailsService {
    /**
     * Bean of user JPA repository.
     */
    @Autowired
    private SQLYajimUserRepository userRepository;

    /**
     * Implementation of {@link UserDetailsService#loadUserByUsername(String)} method.
     * @param username of required user
     * @return Exemplar of {@link UserDetails} class
     * @throws UsernameNotFoundException if User not found
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).map(user -> new User(user.getUsername(),
                user.getPassword(), true, true, true, true,
                rolesToAuthorities(user))
        ).orElseThrow(() -> new UsernameNotFoundException("Couldn't find user with username " + username));
    }

    /**
     * Method converts list of {@link Role} into {@link Set} of {@link GrantedAuthority}.
     * @param user is exemplar of {@link YajimUser} class
     * @return {@link Set} of {@link GrantedAuthority}
     */
    private Set<GrantedAuthority> rolesToAuthorities(YajimUser user) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        user.getRoles().forEach(role -> grantedAuthorities.add(new SimpleGrantedAuthority(role.getName())));
        return grantedAuthorities;
    }
}
