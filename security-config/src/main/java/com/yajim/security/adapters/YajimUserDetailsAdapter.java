package com.yajim.security.adapters;

import com.yajim.messenger.domain.Role;
import com.yajim.messenger.domain.YajimUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link UserDetails} interface.
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize",
        "PMD.ImmutableField", "PMD.MethodArgumentCouldBeFinal",
        "PMD.LongVariable", "PMD.LocalVariableCouldBeFinal"})
public class YajimUserDetailsAdapter implements UserDetails {

    /**
     * User's login.
     */
    private String username;

    /**
     * User's password.
     */
    private String password;

    /**
     * User's set of authorities.
     */
    private Collection<? extends GrantedAuthority> authorities;

    public YajimUserDetailsAdapter(YajimUser user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.authorities = rolesToAuthorities(user);
    }

    /**
     * Method converts list of {@link Role} into {@link Set} of {@link GrantedAuthority}.
     * @param user is exemplar of {@link YajimUser} class
     * @return {@link Set} of {@link GrantedAuthority}
     */
    private Set<GrantedAuthority> rolesToAuthorities(YajimUser user) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        user.getRoles().forEach(role -> grantedAuthorities.add(new SimpleGrantedAuthority(role.getName())));
        return grantedAuthorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
