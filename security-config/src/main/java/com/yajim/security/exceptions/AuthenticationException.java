package com.yajim.security.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom Exception extending {@link RuntimeException}.
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN,
        reason = "Authentication Error. The username or password were incorrect")
public class AuthenticationException extends RuntimeException {
}