package com.yajim.security.filter;

import com.yajim.security.configuration.SecurityConfigProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Filter for corsf.
 */
@SuppressWarnings({"PMD.MethodArgumentCouldBeFinal", "PMD.LongVariable", "PMD.BeanMembersShouldSerialize",
        "PMD.ImmutableField", "PMD.LoggerIsNotStaticFinal", "PMD.ConfusingTernary",
        "PMD.LocalVariableCouldBeFinal", "PMD.UncommentedEmptyMethodBody"})
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SimpleCORSFilter implements Filter {

    /**
     * @param EMPTY empty set.
     */
    private static final Set<String> EMPTY = new HashSet<>();

    /**
     * Property bean to check for allowed origins.
     */
    @Autowired
    private SecurityConfigProperties securityConfigProperties;

    /**
     * @param LOG logger for CORS filter.
     */
    private Logger logger = LoggerFactory.getLogger(SimpleCORSFilter.class);

    /**
     * @param req   HTTP request.
     * @param res   HTTP response.
     * @param chain chain for filter.
     * @throws IOException      if it happens.
     * @throws ServletException if it happens.
     */
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        if (req instanceof HttpServletRequest) {
            HttpServletRequest request = ((HttpServletRequest) req);
            String origin = request.getHeader("origin");
            String contextPath = request.getContextPath();

            if (origin != null) {
                logger.info("Looking for origin " + origin);
                if (getAllowedOrigins(securityConfigProperties.getOrigins()).contains(origin)) {
                    logger.info("Origin " + origin + " found");
                    HttpServletResponse response = (HttpServletResponse) res;
                    response.setHeader("Access-Control-Allow-Origin", origin + contextPath);
                    response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
                    response.setHeader("Access-Control-Max-Age", "3600");
                    response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Authorization, Content-Type");
                    response.setHeader("Access-Control-Allow-Credentials", "true");
                } else {
                    logger.info("Origin " + origin + " not found!");
                }
            } else {
                logger.info("Origin is null!");
            }
        }
        chain.doFilter(req, res);
    }

    /**
     * @param filterConfig initialize config filter.
     */
    public void init(FilterConfig filterConfig) {
    }

    /**
     * destroy config filter.
     */
    public void destroy() {
    }

    /**
     * @param allowedOriginsString allowed strings.
     * @return HashSet of allowed strings.
     */
    private Set<String> parseAllowedOrigins(String allowedOriginsString) {
        if (!StringUtils.isEmpty(allowedOriginsString)) {
            return new HashSet<>(Arrays.asList(allowedOriginsString.split(",")));
        }
        return EMPTY;
    }

    /**
     * @param allowedOriginsString allowed strings.
     * @return set of allowed strings.
     */
    private Set<String> getAllowedOrigins(String allowedOriginsString) {
        return parseAllowedOrigins(allowedOriginsString);
    }

}
