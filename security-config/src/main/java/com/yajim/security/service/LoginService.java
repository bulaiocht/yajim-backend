package com.yajim.security.service;

import com.yajim.messenger.domain.XmppMessage;
import com.yajim.messenger.dto.LoginRequestDTO;
import com.yajim.security.exceptions.AuthenticationException;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.chat2.ChatManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of LoginService.
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.MethodArgumentCouldBeFinal",
        "PMD.LocalVariableCouldBeFinal", "PMD.AvoidCatchingGenericException", "PMD.PreserveStackTrace"})
@Service
public class LoginService {
    /**
     * Password.
     */
    private static final String PASSWORD = "password";
    /**
     * Username.
     */
    private static final String USERNAME = "username";

    /**
     * LoginService messagesForUsername.
     */
    @Autowired
    private Map<String, List<XmppMessage>> messagesForUsername; //NOPMD long varianle name.

    /**
     * Bean of {@link TokenEndpoint} class.
     */
    private final TokenEndpoint endpoint;

    /**
     * Basic constructor.
     *
     * @param endpoint exemplar of {@link TokenEndpoint} class
     */
    @Autowired
    public LoginService(TokenEndpoint endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * Method performs user authorization.
     *
     * @param requestDTO object
     * @return valid valid token inside {@link ResponseEntity} wrapper
     */
    public ResponseEntity<OAuth2AccessToken> authorizeUser(final LoginRequestDTO requestDTO) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(OAuth2Utils.GRANT_TYPE, PASSWORD);
        parameters.put(PASSWORD, requestDTO.getPassword());
        parameters.put(USERNAME, requestDTO.getUsername());
        Collection<GrantedAuthority> collection = new ArrayList<>();
        collection.add((GrantedAuthority) () -> "ROLE_TRUSTED_CLIENT");
        final Authentication principal = new UsernamePasswordAuthenticationToken("trusted-app",
                "secret", collection);
        try {
            SecurityContextHolder.getContext().setAuthentication(principal);
            return endpoint.postAccessToken(principal, parameters);
        } catch (Exception e) {
            throw new AuthenticationException();
        }
    }

    /**
     * Register new chat listener for xmmp connection.
     * That listen all incoming messages and store them in memory.
     */
    public void registerChatListener(AbstractXMPPConnection connection, String username) {
        ChatManager chatManager = ChatManager.getInstanceFor(connection);
        chatManager.addIncomingListener((from, incomingMessage, fromChat) -> {
            XmppMessage newMessage = new XmppMessage();
            if (!messagesForUsername.containsKey(username)) {
                messagesForUsername.put(username, new ArrayList<>());
            }
            newMessage.setFromJid(from.asBareJid().toString());
            newMessage.setNew(true);
            newMessage.setMessage(incomingMessage.getBody());
            newMessage.setDate(LocalDateTime.now());
            List<XmppMessage> usernameAllMessages = messagesForUsername.get(username); //NOPMD long varianle name.
            usernameAllMessages.add(newMessage);
        });
    }
}
