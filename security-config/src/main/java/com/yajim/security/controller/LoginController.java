package com.yajim.security.controller;

import com.yajim.messenger.dto.LoginRequestDTO;
import com.yajim.messenger.service.UserService;
import com.yajim.messenger.validation.LoginRequestDTOValidation;
import com.yajim.security.service.LoginService;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


/**
 * Login controller.
 */
@RestController
@RequestMapping(path = "/signin")
@SuppressWarnings({"PMD.BeanMembersShouldSerialize",
        "PMD.MethodArgumentCouldBeFinal"})
public class LoginController {

    /**
     * {@link UserService} variable for controller.
     */
    private UserService userService; //NOPMD non-transient.

    /**
     * LoginController loginService.
     * {@link LoginService} variable for controller.
     */
    private final LoginService loginService; //NOPMD non-transient.

    /**
     * LoginController xmppConnectionMap.
     */
    @Autowired
    private Map<String, AbstractXMPPConnection> xmppConnectionMap;

    /**
     * Basic constructor.
     *
     * @param userService  exemplar of {@link UserService} class.
     * @param loginService exemplar of {@link LoginService} class.
     */
    @Autowired
    public LoginController(UserService userService, LoginService loginService) {
        this.userService = userService;
        this.loginService = loginService;
    }

    /**
     * Authenticate user if its username and password correct and valid.
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<OAuth2AccessToken> authenticateUser(
            @Valid @RequestBody LoginRequestDTO requestDTO, HttpSession httpSession) {
        if (!userService.userIsPresent(requestDTO)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            String username = requestDTO.getUsername(); //NOPMD could be final.
            httpSession.setAttribute("username", username);
            AbstractXMPPConnection connection = userService.getConnectionByUsername(requestDTO.getUsername(), //NOPMD .
                    requestDTO.getPassword());
            xmppConnectionMap.put(username, connection);
            loginService.registerChatListener(connection, username);
        } catch (IOException | InterruptedException | XMPPException | SmackException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return loginService.authorizeUser(requestDTO);
    }

    /**
     * Initiating WebDataBinder for checking login form.
     *
     * @param binder new {@link DataBinder}
     */
    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        binder.setValidator(new LoginRequestDTOValidation());
    }
}
