package com.yajim.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Should be removed by a real controller.
 */
@RestController
@RequestMapping(path = "/public")
public class PublicController {

    /**
     * Stub REST endpoint.
     * @return "hello".
     */
    @GetMapping
    public String hello() {
        return "Hello";
    }
}


