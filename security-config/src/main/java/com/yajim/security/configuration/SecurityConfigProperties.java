package com.yajim.security.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Security config class.
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "cors.allowed")
@Component
public class SecurityConfigProperties {

    /**
     * @param origin origin properties.
     */
    private String origins;

}