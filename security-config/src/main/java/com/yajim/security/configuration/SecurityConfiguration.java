package com.yajim.security.configuration;

import com.yajim.security.adapters.YajimUserDetailsServiceAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Configuration class for web security.
 */
@SuppressWarnings({"PMD.SignatureDeclareThrowsException", "PMD.MethodArgumentCouldBeFinal"})
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * This method configuring service for retrieving user details.
     * @param auth builder for oAuth.
     * @throws Exception if happens.
     */
    @Autowired
    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }

    //TODO Remember this place!! here you need to add allowed for everyone
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.logout().and().antMatcher("/stub").authorizeRequests()
                .antMatchers("/signin", "/public", "/app/**"/*, "/messages"*/).permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("**/*.html").permitAll()
                .antMatchers("**/*.css").permitAll()
                .antMatchers("**/*.js").permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf().disable();
    }

    /**
     * Method to obtain a bean of your custom {@link UserDetailsService} implementation.
     * @return an instance of {@link UserDetailsService} interface
     */
    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        return new YajimUserDetailsServiceAdapter();
    }
}
