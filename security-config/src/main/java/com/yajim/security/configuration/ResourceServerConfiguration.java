package com.yajim.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

/**
 * Configuration class for resource server.
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.MethodArgumentCouldBeFinal",
        "PMD.MethodArgumentCouldBeFinal", "PMD.UselessParentheses", "PMD.LocalVariableCouldBeFinal",
        "PMD.SignatureDeclareThrowsException"})
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    /**
     * @param resourceId resource id.
     */
    @Value("${resource.id:spring-boot-application}")
    private String resourceId;

    /**
     * Configure resources.
     *
     * @param resources security resources.
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        // @formatter:off
        resources.resourceId(resourceId);
        // @formatter:on
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http.requestMatcher(new OAuthRequestedMatcher())
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .anyRequest().authenticated();
        // @formatter:on
    }

    /**
     * Class for matching request to oAuth token.
     */
    private static class OAuthRequestedMatcher implements RequestMatcher {
        public boolean matches(HttpServletRequest request) {
            String auth = request.getHeader("Authorization");
            // Determine if the static request contained an OAuth Authorization
            boolean haveOauthToken = (auth != null) && auth.startsWith("Bearer");
            boolean haveAccessToken = request.getParameter("access_token") != null;
            return haveOauthToken || haveAccessToken;
        }
    }

}
