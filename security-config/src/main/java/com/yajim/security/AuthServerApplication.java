package com.yajim.security;

import com.yajim.messenger.UserApp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * Access point for Spring Boot application.
 */
@EnableAutoConfiguration
@Import(UserApp.class)
@EnableResourceServer
@EnableConfigurationProperties
@SuppressWarnings({"PMD.UseUtilityClass", "PMD.MethodArgumentCouldBeFinal"}) //Spring things.
@SpringBootApplication
public class AuthServerApplication {

    /**
     * Main method.
     * @param args arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
    }

}