package com.yajim.messenger.ports;

import com.yajim.messenger.dto.XmppUserDTO;
import org.jivesoftware.smack.AbstractXMPPConnection;

import java.util.Set;

/**
 * Port for getting roster.
 */
public interface RosterPort {
    /**
     * Method for getting roster by user xmpp connection.
     * @param connection input user xmpp connection.
     * @return set of user contacts.
     */
    Set<XmppUserDTO> getRoster(AbstractXMPPConnection connection);
}
