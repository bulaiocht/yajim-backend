package com.yajim.messenger.contoller;

import com.yajim.messenger.dto.XmppUserDTO;
import com.yajim.messenger.service.RosterService;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpSession;

/**
 * Login controller.
 */
@RestController
@RequestMapping(path = "/roster")
@SuppressWarnings("PMD.LocalVariableCouldBeFinal")
public class RosterController {

    /**
     * RosterService variable for controller.
     */
    @Autowired
    private RosterService rosterService; // NOPMD non transient.\

    /**
     * RosterController xmppConnectionMap.
     */
    @Autowired
    private Map<String, AbstractXMPPConnection> xmppConnectionMap; //NOPMD Found non-transient, non-static member.

    /**
     * Returns list of user's contacts by current user session.
     */
    @GetMapping
    public ResponseEntity<Set<XmppUserDTO>> getRoster(HttpSession session) { //NOPMD could be final.
        String username = (String) session.getAttribute("username");
        Set<XmppUserDTO> xmppUserDTOSet = rosterService.getRoster(xmppConnectionMap.get(username));
        return new ResponseEntity<>(xmppUserDTOSet, HttpStatus.OK);
    }
}
