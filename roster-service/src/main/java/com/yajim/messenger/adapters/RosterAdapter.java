package com.yajim.messenger.adapters;

import com.yajim.messenger.dto.XmppUserDTO;
import com.yajim.messenger.ports.RosterPort;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by hladikov.denys on 14.07.17.
 */
@SuppressWarnings({"PMD.AvoidInstantiatingObjectsInLoops",
        "PMD.LocalVariableCouldBeFinal", "PMD.MethodArgumentCouldBeFinal"})
public class RosterAdapter implements RosterPort {

    /**
     * Method for getting roster by user xmpp connection.
     *
     * @param connection input user xmpp connection.
     * @return set of user contacts.
     */
    public Set<XmppUserDTO> getRoster(AbstractXMPPConnection connection) {
        Roster roster = Roster.getInstanceFor(connection);
        Collection<RosterEntry> entries = roster.getEntries();

        Set<XmppUserDTO> xmppUserDTOSet = new HashSet<>(entries.size());

        entries.forEach(rosterEntry -> xmppUserDTOSet.add(
                new XmppUserDTO(rosterEntry.getJid().asUnescapedString(),
                rosterEntry.getName(), roster.getPresence(rosterEntry.getJid()).getStatus())));

        return xmppUserDTOSet;
    }

}
