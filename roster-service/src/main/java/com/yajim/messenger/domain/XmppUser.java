package com.yajim.messenger.domain;

import lombok.Value;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Main user entity.
 */
@Entity
@Value
public class XmppUser {

    /**
     * XmppUser primary key.
     */
    @Id
    @GeneratedValue
    private Long id; //NOPMD id short variable?

    /**
     * XmppUser jid.
     */
    private String jid;

    /**
     * XmppUser username.
     */
    private String username;

    /**
     * XmppUser status (online, offline).
     */
    private String status;
}
