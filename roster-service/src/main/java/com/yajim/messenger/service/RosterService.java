package com.yajim.messenger.service;

import com.yajim.messenger.adapters.RosterAdapter;
import com.yajim.messenger.dto.XmppUserDTO;
import com.yajim.messenger.ports.RosterPort;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Service for getting user's roster.
 */
@Service
@SuppressWarnings({"PMD.BeanMembersShouldSerialize",
        "PMD.LocalVariableCouldBeFinal", "PMD.MethodArgumentCouldBeFinal"})
public class RosterService {
    /**
     * Implementing method adapter.
     */
    private final RosterPort port = new RosterAdapter();

    /**
     * Method for getting roster by user xmpp connection.
     * @param connection input user xmpp connection.
     * @return set of user contacts.
     */
    public Set<XmppUserDTO> getRoster(AbstractXMPPConnection connection) {
        return port.getRoster(connection);
    }
}
