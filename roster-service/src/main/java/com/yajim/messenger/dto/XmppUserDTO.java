package com.yajim.messenger.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for User's contact.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class XmppUserDTO {
    /**
     * Contact JID.
     */
    private String jid;

    /**
     * Contact username.
     */
    private String username;

    /**
     * Contact status (online, offline...,  none if not assigned).
     */
    private String status;
}
