package com.yajim.messenger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Roster application.
 */
@EnableConfigurationProperties
@SpringBootApplication
@SuppressWarnings("PMD.UseUtilityClass")
public class RosterApp {

    /**
     * @param args commandline arguments.
     */
    public static void main(final String[] args) {
        SpringApplication.run(RosterApp.class, args);
    }
}
